import 'package:flutter/material.dart';
import 'package:flutter_reto5/vistaPrincipal.dart';

void main() => runApp(MyApp());
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
  //Inicializacion del control para el tema oscuro
  bool themeSwitch = false;
  dynamic themeColors() {
    if (themeSwitch) {
      return Colors.grey[850];
    } else {
      return Color.fromRGBO(86, 130, 163, 1);
    }
  }
  //Fin de la inicializacion

class _MyAppState extends State<MyApp> {
  TextEditingController _controller;
  @override
  //Iconicalizacion para el control de la barra de busqueda
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }
  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Text('Telegram');
  Widget cusBack;
  //Fin de la inicializacion

  //Iconicalizacion para controlar la visibilidad de las cuentas
  bool cuentasSwitch = false;
  //Fin de la inicializacion

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Telegram',
      theme: themeSwitch ? ThemeData.dark() : ThemeData.light(),
      debugShowCheckedModeBanner: false,
      home: _home(),
    );
  }
  Widget _home(){
    return Scaffold(
      appBar: AppBar(
          leading: cusBack,
          backgroundColor: themeColors(),
          title: cusSearchBar,
          actions: <Widget>[
            IconButton(
              icon: cusIcon,
              padding: EdgeInsets.all(8.0),
              onPressed: (){
                setState(() {
                  if(this.cusIcon.icon==Icons.search){
                    this.cusIcon = Icon(Icons.clear);
                    this.cusBack = _back();
                    this.cusSearchBar = TextField(
                      controller: _controller,
                      textInputAction: TextInputAction.go,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Buscar',
                        hintStyle: TextStyle(
                        color: Colors.white60,
                        fontSize: 20,
                      ),
                      ),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    );
                  }else{
                    _controller.clear();
                  }
                });
              }
            ),
          ],
        ),
        drawer: _draw(),
        body: VistaPrincipal(),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.create,color: Colors.white,),
            backgroundColor: Color(0xFF65a9e0),
            onPressed: (){}),
    );
  }
  //Widget para salir de la barra de busqueda
  Widget _back(){
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: (){
        setState(() {
            this.cusIcon = Icon(Icons.search);
            this.cusSearchBar = Text('Telegram');
            this.cusBack = null;
        });
      },
    );
  }
  Widget _draw(){
    return Drawer(
      child: ListView(
        children: <Widget>[
          _account(),
          _cuentas(),
          _contenedor(Icons.group, 'Nuevo grupo'),
          _contenedor(Icons.lock, 'Nuevo chat secreto'),
          _contenedor(Icons.notifications, 'Nuevo canal'),
          _contenedor(Icons.contacts, 'Contactos'),
          _contenedor(Icons.phone, 'Llamadas'),
          _contenedor(Icons.bookmark_border, 'Mensajes guardados'),
          _contenedor(Icons.settings, 'Ajustes'),
          Divider(color: Colors.black54),
          _contenedor(Icons.person_add, 'Invitar amigos'),
          _contenedor(Icons.help_outline, 'Preguntas frecuentes')
        ],
      ),
    );
  }
  Widget _cuentas(){
    return Visibility(
      child: Column(
        children: <Widget>[
          //_accounts(),
          _avatar(),
          _contenedor(Icons.add, 'Añadir Cuenta'),
          Divider(color: Colors.black54),
        ],
      ),
      visible: cuentasSwitch,
    );
  }
  Widget _accounts(){
    return ListTile(
              leading: CircleAvatar(
                radius: 20,
                backgroundColor: Colors.orange,
                child: Text('JM',style: TextStyle(color: Colors.white)), 
              ),
              title: Text('Joel Mendoza',style: TextStyle(
                fontSize: 16, 
                fontWeight: FontWeight.w500,
                color: themeSwitch ? Colors.white : Colors.black87
              )),
              onTap: (){},
            );
  }
  Widget _avatar(){
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: _accounts(),
        ),
        Align(
          alignment: Alignment.bottomLeft + Alignment(0.3, 0),
          child: _check(),
        )
      ],
    );
  }
  Widget _check(){
    return Container(
      width: 20,
      height: 20,
      margin: EdgeInsets.only(top: 32),
      decoration: BoxDecoration(
        color: themeSwitch ? Colors.blue : Colors.green,
        border: Border.all(
          color: themeSwitch ? Colors.grey[850] : Colors.white,
          width: 2,
        ),
        borderRadius: BorderRadius.all(Radius.circular(100))
      ),
      child: Center(child: Icon(Icons.check,color: Colors.white,size: 14,)),
    );
  }
  Widget _account() {
    return DrawerHeader(
      decoration: BoxDecoration(
        color: themeSwitch ? Colors.black87 : Color.fromRGBO(86, 130, 163, 1),
      ),
      child: Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft + Alignment(0, 0.2),
          child: CircleAvatar(
            backgroundColor: themeSwitch ? Color.fromRGBO(86, 130, 163, 1) : Color(0xFF255674),
            radius: 35.0,
            child: Text('JM',style: TextStyle(fontSize: 20,color: Colors.white)),
          )
        ),
        Align(
          alignment: Alignment.bottomLeft + Alignment(0, -0.3),
          child: Text('Joel Mendoza', style: TextStyle(color: Colors.white))
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Text('+591 (7) 894-4081', style: TextStyle(color: Color.fromRGBO(255, 255, 255, 0.5)))
        ),
        Align(
          alignment: Alignment.topRight + Alignment(0, 0.2),
          child: IconButton(
            onPressed: () {
              setState(() {
                themeSwitch = !themeSwitch;
              });
            },
            icon: Icon(
                    Icons.brightness_3,
                    color: Colors.white,
                  )
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          // child: Icon( Icons.keyboard_arrow_down,color: Colors.white,)
          child: IconButton(
            onPressed: (){
              setState(() {
                cuentasSwitch = !cuentasSwitch;
              });
            },
            icon: Icon(
                   cuentasSwitch ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                   //Icons.keyboard_arrow_down,
                   color: Colors.white,
                  )
          ),
        ),
      ],
    )
    );
  }
  Widget _contenedor(IconData icon, String nombrec) {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.only(top: 10, bottom: 10, left: 15),
        child: Row(
          children: <Widget>[
            Icon(icon, color: themeSwitch ? Colors.white30 : Colors.black54),
            SizedBox(
              width: 20,
            ),
            SizedBox(width: 10.0),
            Text(nombrec, 
              style: TextStyle(
                fontSize: 16, 
                fontWeight: FontWeight.w500,
                color: themeSwitch ? Colors.white : Colors.black87
              )
            )
          ],
        ),
      ),
    );
  }
}