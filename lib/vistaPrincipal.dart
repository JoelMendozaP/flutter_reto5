import 'package:flutter/material.dart';
import 'main.dart';
class VistaPrincipal extends StatefulWidget {
  @override
  _VistaPrincipalState createState() => _VistaPrincipalState();
}

class _VistaPrincipalState extends State<VistaPrincipal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          _contenido("assets/img/1.jpg",'Ronnie Coleman'),
          Divider(color: Colors.black26),
          _contenido("assets/img/2.jpg",'Michelle Lewin'),
          Divider(color: Colors.black26),
          _contenido("assets/img/3.jpg",'Bahar Naviera'),
          Divider(color: Colors.black26),
          _contenido("assets/img/4.jpg",'Gero'),
          Divider(color: Colors.black26),
          _contenido("assets/img/anllela.jpg",'Anllela Sagra'),
          Divider(color: Colors.black26),
          _contenido("assets/img/1.jpg",'Ronnie Coleman'),
          Divider(color: Colors.black26),
          _contenido("assets/img/2.jpg",'Michelle Lewin'),
          Divider(color: Colors.black26),
          _contenido("assets/img/3.jpg",'Bahar Naviera'),
          Divider(color: Colors.black26),
          _contenido("assets/img/4.jpg",'Gero'),
          Divider(color: Colors.black26),
          _contenido("assets/img/anllela.jpg",'Anllela Sagra'),
        ],
      ),
    );
  }
  Widget _contenido(String url,String name){
    return ListTile(
        leading: _img(url),
        title: _userName(name),
        subtitle: _detalle(),
        enabled: true,
        trailing: _hora(),
    );
  }
  Widget _img(String url) {
    return Container(
      width: 55.0,
      height: 55.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.cover, image: AssetImage(url))),
    );
  }
  Widget _userName(String name) {
    return Container(
      //margin: EdgeInsets.only(bottom: 5),
      child: Text(
        name,
        style: TextStyle(
          fontSize: 17.0,
          color: themeSwitch ? Colors.white : Colors.black,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
  Widget _detalle() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Text(
        'Lorem Ipsum is simply dum...',
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 16.0, color: Color(0xFFa3a5a7)),
      ),
    );
  }
  Widget _hora() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('10:59',style: TextStyle(fontSize: 12.0, color: Color(0xFFa3a5a7))),
        _nroMensajes()
      ],
    );
  }
  Widget _nroMensajes(){
    return Container(
      width: 25,
      height: 25,
      //margin: EdgeInsets.only(top: 18),
      decoration: BoxDecoration(
        color: themeSwitch ? Colors.blue : Colors.green,
        borderRadius: BorderRadius.all(Radius.circular(100))
      ),
      child: Center(child: Text('3',style: TextStyle(color: Colors.white))),
    );
  }
}
